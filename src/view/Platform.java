package view;

import controller.*;
import controller.DrawController;
import model.objects.*;
import model.characters.*;

import java.awt.*;
import java.util.*;
import java.util.List;
import javax.swing.*;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int FIRST_BACKGROUND_POSITION = 800;
    private static final int SECOND_BACKGROUND_POSITION = 800;
    private static final int POSITION_TO_CHECK = 4600;
    private static final int INIT_FIRSTBACKGROUND = -50;
    private static final int INIT_SECONDBACKGROUND = 750;
    private static final int X_POSITION=1;
    private static final int X_POSITION_BEGINNING=0;
    private static final int INIT_MOVEMENT=0;
    private static final int FLOOR_OFFSET_Y=293;
    private static final int HEIGHT_LIMIT = 0;

    private Background firstImageBackground;
    private Background secondImageBackground;
    private Castle castle;
    private Flag flag;
    private StartPoint start;

    private int firstBackgroundPosX;
    private int secondBackgroundPosX;
    private int movement;
    private int xPosition;
    private int floorOffsetY;
    private int heightLimit;

    private Mario mario;
    private Mushroom mushroom;
    private Turtle turtle;

    private FinalCastle finalCastle;
    private DrawController drawController;
    private ContactController contactController;
    private ObjectsMoverController objectsMoverController;
    private List<GameObject> objects;
    private List<Coin> coins;

    public Platform() {
        super();
        this.firstBackgroundPosX = INIT_FIRSTBACKGROUND;
        this.secondBackgroundPosX = INIT_SECONDBACKGROUND;
        this.initializeControllers();
        this.movement = INIT_MOVEMENT;
        this.xPosition = -X_POSITION;
        this.floorOffsetY = FLOOR_OFFSET_Y;
        this.heightLimit = HEIGHT_LIMIT;
        this.firstImageBackground = new Background(0,0);
        this.secondImageBackground = new Background(0,0);
        this.castle = new Castle();
        this.flag=new Flag();
        this.finalCastle = new FinalCastle();
        this.start = new StartPoint();
        this.drawCharacters();

        objects = new ArrayList<>();
        coins = new ArrayList<>();
        ObjectsHandler objectsHandler =new ObjectsHandler();
        this.objects= objectsHandler.getObjectsList();
        CoinHandler coinHandler =new CoinHandler();
        this.coins = coinHandler.getPiecesList();

        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addKeyListener(new Keyboard());
    }

    public final Mario getMario() {
        return mario;
    }

    public final int getFloorOffsetY() {
        return floorOffsetY;
    }

    public final int getHeightLimit() {
        return heightLimit;
    }

    public final int getMovement() {
        return movement;
    }

    public final int getxPosition() {
        return xPosition;
    }

    public final void setSecondBackgroundPosX(int secondBackgroundPositionX) {
        this.secondBackgroundPosX = secondBackgroundPositionX;
    }

    public final void setFloorOffsetY(int floorOffsetToSet) {
        this.floorOffsetY = floorOffsetToSet;
    }

    public final void setHeightLimit(int heightLimitMax) {
        this.heightLimit = heightLimitMax;
    }

    public final void setxPosition(int position) {
        this.xPosition = position;
    }

    public final void setMovement(int movementToSet) {
        this.movement = movementToSet;
    }

    public final void setFirstBackgroundPosX(int x) {
        this.firstBackgroundPosX = x;
    }

    public final void updateBackgroundOnMovement() {
        if (this.xPosition >= X_POSITION_BEGINNING && this.xPosition <= POSITION_TO_CHECK) {
            this.xPosition = this.xPosition + this.movement;
            this.firstBackgroundPosX = this.firstBackgroundPosX - this.movement;
            this.secondBackgroundPosX = this.secondBackgroundPosX - this.movement;
        }
        if(this.firstBackgroundPosX==FIRST_BACKGROUND_POSITION || this.firstBackgroundPosX==-FIRST_BACKGROUND_POSITION){
            this.firstBackgroundPosX*=-1;
        }
        if(this.secondBackgroundPosX==SECOND_BACKGROUND_POSITION || this.secondBackgroundPosX==-SECOND_BACKGROUND_POSITION){
            this.secondBackgroundPosX*=-1;
        }
    }

    public final void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = g;

        contactController.checkContactsWithObjects((ArrayList<GameObject>) this.objects,mario,mushroom,turtle);
        contactController.checkContactWithCoin((ArrayList<Coin>) this.coins,mario);
        contactController.checkContactAmongstCharacters(this.mushroom,this.turtle,this.mario);

        this.updateBackgroundOnMovement();
        if (this.xPosition >= X_POSITION_BEGINNING && this.xPosition <= POSITION_TO_CHECK) {
            objectsMoverController.moveObjects(this.objects);
            objectsMoverController.movePieces(this.coins);
            objectsMoverController.moveTurtleAndMushroom(this.mushroom,this.turtle);
        }
        this.drawBackground(g2);
        this.drawCharacters(g2);
    }

    private void drawCharacters(Graphics g2) {
        drawController.drawMario(g2,this.mario);
        drawController.drawMushroom(g2,this.mushroom);
        drawController.drawTurtle(g2,this.turtle);
    }

    private void drawBackground(Graphics g2) {
        drawController.drawBackground(g2,this.firstImageBackground);
        drawController.drawBackground(g2,this.secondImageBackground);
        drawController.drawCastle(g2,this.castle,this.xPosition);
        drawController.drawStartPoint(g2,this.start,this.xPosition);
        drawController.drawObjects(g2, this.objects);
        drawController.drawPieces(g2, this.coins);
        drawController.drawFlag(g2,this.flag,xPosition);
        drawController.drawFinalCastle(g2,this.finalCastle,this.xPosition);
    }

    private void initializeControllers(){
        this.drawController =new DrawController();
        this.contactController =new ContactController();
        this.objectsMoverController =new ObjectsMoverController();
    }

    private void drawCharacters(){
        mario = new Mario(300, 245);
        mushroom = new Mushroom(800, 263);
        turtle = new Turtle(950, 243);
    }

}
