package view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    private static final String JUMP_SOUND = "/resources/audio/jump.wav";
    private static final int FIRST_BACKGROUND_POSITION = -50;
    private static final int SECOND_BACKGROUND_POSITION = 750;
    private static final int MOVEMENT = 1;
    public static final int BACKGROUND_INITIAL_POSITION_X = 4600;
    public static final int FIRSTBACKGROUND_INITIAL_POSITION_X = 0;

    @Override
    public final void keyPressed(KeyEvent event) {
        if (Main.getScene().getMario().isAlive()) {
            switch (event.getKeyCode()){
                case KeyEvent.VK_RIGHT:
                    if (Main.getScene().getxPosition() == FIRSTBACKGROUND_INITIAL_POSITION_X-MOVEMENT) {
                        this.setBackgroundPosition(FIRSTBACKGROUND_INITIAL_POSITION_X,FIRST_BACKGROUND_POSITION,SECOND_BACKGROUND_POSITION);
                    }
                    this.setMovement(true,true,MOVEMENT);
                    break;
                case KeyEvent.VK_LEFT:
                    if (Main.getScene().getxPosition() == BACKGROUND_INITIAL_POSITION_X+MOVEMENT) {
                        this.setBackgroundPosition(BACKGROUND_INITIAL_POSITION_X,FIRST_BACKGROUND_POSITION,SECOND_BACKGROUND_POSITION);
                    }
                    this.setMovement(true,false,-MOVEMENT);
                    break;
                case KeyEvent.VK_UP:
                    Main.getScene().getMario().setJumping(true);
                    Audio.playSound(JUMP_SOUND);
                    break;
                case KeyEvent.VK_R:
                    String[] args = {};
                    Main.main(args);
                    Score.getInstance().resetScore();
                    break;
            }
        }
    }

    private void setMovement(boolean isMoving, boolean isMovingToRight, int movement) {
        Main.getScene().getMario().setMoving(isMoving);
        Main.getScene().getMario().setToRight(isMovingToRight);
        Main.getScene().setMovement(movement);
    }

    private void setBackgroundPosition(int xPosition, int firstBackgroundPosition,int secondBackgroundPosition){
        Main.getScene().setxPosition(xPosition);
        Main.getScene().setFirstBackgroundPosX(firstBackgroundPosition);
        Main.getScene().setSecondBackgroundPosX(secondBackgroundPosition);
    }


    @Override
    public final void keyReleased(KeyEvent event) {
        Main.getScene().getMario().setMoving(false);
        Main.getScene().setMovement(0);
    }

    @Override
    public void keyTyped(KeyEvent event) {
    }

}
