package view;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Refresh implements Runnable {

    private static final int PAUSE = 3;
    private static final Logger LOGGER = Logger.getLogger(Audio.class.getName());

    public final void run() {
        while (true) {
            Main.getScene().repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                LOGGER.log(Level.WARNING, "Refresh exception", e);
            }
        }
    }
} 
