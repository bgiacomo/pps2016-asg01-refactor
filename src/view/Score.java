package view;

/**
 * Created by Giacomo on 14/03/2017.
 */
public class Score {
    private static Score ourInstance = new Score();
    private int score = 0;

    public static Score getInstance() {
        return ourInstance;
    }


    public final int getScore(){
        return this.score;
    }

    public final void incrementScore(){
        this.score++;
    }

    public final void resetScore(){
        this.score=0;
    }
}
