package view;

import javax.swing.*;
import java.awt.*;

public class Main {

    private static final String WINDOW_TITLE = "Super Mario";
    private static Platform scene;
    private static JLabel label;

    public static void main(String[] args) {
       createScoreLabel();
        scene = new Platform();
        scene.add(label,BorderLayout.LINE_END);
        new GameWindow(WINDOW_TITLE, scene);
        startTimer();
    }

    private static void startTimer(){
        Thread timer = new Thread(new Refresh());
        timer.start();
    }

    private static void createScoreLabel(){

        label=new JLabel("  Score:  "+new Score().getScore());
        label.setOpaque(true);
        label.setBackground(Color.green);

    }

    public static Platform getScene() {
        return scene;
    }

    public static JLabel getLabel(){return label;}
}
