package model.objects;

import model.BackgroundElement;
import utils.Res;

/**
 * Created by Giacomo on 12/03/2017.
 */
public class StartPoint extends BackgroundElement {

    private static final int X = 220;
    private static final int Y = 234;

    public StartPoint() {
        super(X, Y, 0, 0, Res.START_ICON);
    }
}
