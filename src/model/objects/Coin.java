package model.objects;

import view.Audio;
import utils.Res;
import utils.Utils;

import java.awt.Image;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Coin extends GameObject implements Runnable {

    private static final int WIDTH = 30;
    private static final int HEIGHT = 30;
    private static final int PAUSE = 10;
    private static final int FLIP_FREQUENCY = 100;
    private static final Logger LOGGER = Logger.getLogger(Audio.class.getName());

    private int counter;

    public Coin(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.imageObject = Utils.getImage(Res.IMG_PIECE1);
    }

    public final Image imageOnMovement() {
        return Utils.getImage(++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

    @Override
    public final void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                LOGGER.log(Level.WARNING, "Coin exception", e);
            }
        }
    }
}
