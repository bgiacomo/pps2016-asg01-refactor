package model.objects;

import model.BackgroundElement;
import utils.Res;

/**
 * Created by Giacomo on 12/03/2017.
 */
public class Flag extends BackgroundElement{

    private final static int X=4650;
    private final static int Y=115;


    public Flag() {
        super(X, Y, 0, 0, Res.IMG_FLAG);
    }
}
