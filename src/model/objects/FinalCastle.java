package model.objects;

import model.BackgroundElement;
import utils.Res;

/**
 * Created by Giacomo on 12/03/2017.
 */
public class FinalCastle extends BackgroundElement {

    private final static int X=4850;
    private final static int Y=145;

    public FinalCastle() {
        super(X,Y,0,0,Res.IMG_CASTLE_FINAL);
    }
}
