package model.objects;

import model.BackgroundElement;
import utils.Res;

/**
 * Created by Giacomo on 12/03/2017.
 */
public class Background extends BackgroundElement {

    private int x;

    public Background(int xObject, int yObject) {
        super(xObject, yObject, 0, 0, Res.IMG_BACKGROUND);
    }

    @Override
    public void setX(int xBackground) {
        this.x = xBackground;
    }

    @Override
    public int getX() {
        return x;
    }
}
