package model.objects;

import model.BackgroundElement;
import utils.Res;

/**
 * Created by Giacomo on 12/03/2017.
 */
public class Castle  extends BackgroundElement{

    private final static int X=10;
    private final static int Y=10;

    public Castle() {
        super(X, Y, 0, 0, Res.IMG_CASTLE);
    }

}
