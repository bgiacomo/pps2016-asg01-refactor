package model.objects;

import java.awt.Image;

import model.GameElement;

public class GameObject extends GameElement {

    protected Image imageObject;

    public GameObject(int xObject, int yObject, int widthObject, int heightObject) {
       super(xObject,yObject,widthObject,heightObject);
    }

    public final Image getImageObject() {
        return imageObject;
    }


}
