package model;

import model.objects.GameObject;
import utils.Utils;

import java.awt.*;

/**
 * Created by Giacomo on 12/03/2017.
 */
public class BackgroundElement extends GameObject {

    private Image image;

    public BackgroundElement(int xObject, int yObject, int widthObject, int heightObject, String imageBackground) {
        super(xObject, yObject, widthObject, heightObject);
        this.image= Utils.getImage(imageBackground);
    }

    public final Image getImage() {
        return image;
    }
}
