package model;

/**
 * Created by Giacomo on 11/03/2017.
 */
public interface Element {

    int getX();

    int getY();

    int getWidth();

    int getHeight();

    void setX(int xCharacter);

    void setY(int yCharacter);

    void move();

}
