package model.characters;

import java.awt.Image;
import utils.Res;
import utils.Utils;

public class Turtle extends AbstractEnemy {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    public Turtle(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
    }

    public final Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}
