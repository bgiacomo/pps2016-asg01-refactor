package model.characters;

import java.awt.*;

import model.objects.Coin;
import view.Main;
import model.objects.GameObject;
import utils.Res;
import utils.Utils;
import view.Score;

public class Mario extends BasicCharacter {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int HEIGHT_LIMIT=0;
    private static final int JUMPING_LIMIT = 42;
    private static final int JUMP_OFFSET = 4;
    private static final int JUMPING_EXTENT = 0;

    private boolean isJumping;
    private int jumpingExtent;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.isJumping = false;
        this.jumpingExtent = JUMPING_EXTENT;
    }

    public final boolean isJumping() {
        return isJumping;
    }

    public final void setJumping(boolean jump) {
        this.isJumping = jump;
    }

    public final Image doJump() {
        String stringJump;

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > Main.getScene().getHeightLimit()) {
                this.setY(this.getY() - JUMP_OFFSET);
            } else {
                this.jumpingExtent = JUMPING_LIMIT;
            }

        } else if (this.getY() + this.getHeight() < Main.getScene().getFloorOffsetY()) {
            this.setY(this.getY() + 1);
        } else {
            this.isJumping = false;
            this.jumpingExtent = JUMPING_EXTENT;
        }
        stringJump = this.isToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        return Utils.getImage(stringJump);
    }

    public final void contact(GameObject gameObject) {
        if (this.hitAhead(gameObject) && this.isToRight() || this.hitBack(gameObject) && !this.isToRight()) {
            Main.getScene().setMovement(0);
            this.setMoving(false);
        }

        if (this.hitBelow(gameObject) && this.isJumping) {
            Main.getScene().setFloorOffsetY(gameObject.getY());
        } else if (!this.hitBelow(gameObject)) {
            Main.getScene().setFloorOffsetY(FLOOR_OFFSET_Y_INITIAL);
            this.checkJumpAndHit(gameObject);
        }
    }

    public final void contact(BasicCharacter character) {
        if (this.hitAhead(character) || this.hitBack(character)) {
            if (character.alive) {
                this.setMoving(false);
                this.setAlive(false);
                String[] args = {};
                Main.main(args);
                Score.getInstance().resetScore();
            } else {
                this.alive = true;
            }
        } else if (this.hitBelow(character)) {
            character.setMoving(false);
            character.setAlive(false);
        }
    }

    private void checkJumpAndHit(GameObject gameObject){
        if (!this.isJumping) {
            this.setY(MARIO_OFFSET_Y_INITIAL);
        }

        if (hitAbove(gameObject)) {
            Main.getScene().setHeightLimit(gameObject.getY() + gameObject.getHeight());
        } else if (!this.hitAbove(gameObject) && !this.isJumping) {
            Main.getScene().setHeightLimit(HEIGHT_LIMIT);
        }
    }

    public final boolean contactCoin(Coin coin) {
        return this.hitBack(coin) || this.hitAbove(coin) || this.hitAhead(coin)
                || this.hitBelow(coin);

    }

}
