package model.characters;

import java.awt.Image;

import model.GameElement;
import model.objects.GameObject;
import utils.Res;
import utils.Utils;

public class BasicCharacter extends GameElement implements Character {

    private static final int PROXIMITY_MARGIN = 10;
    private static final int HIT_MARGIN = 5;
    private boolean moving;
    private boolean toRight;
    private int counter;
    protected boolean alive;
    private int margin=0;

    public BasicCharacter(int xCharacter, int yCharacter, int widthCharacter, int heightCharacter) {
        super(xCharacter,yCharacter,widthCharacter,heightCharacter);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }

    public final boolean isAlive() {
        return alive;
    }

    public final boolean isToRight() {
        return toRight;
    }

    public final void setAlive(boolean aliveCharacter) {
        this.alive = aliveCharacter;
    }

    public final void setMoving(boolean movingCharacter) {
        this.moving = movingCharacter;
    }

    public final void setToRight(boolean toRightCharacter) {
        this.toRight = toRightCharacter;
    }

    public final Image walk(String name, int frequency) {
        String str = Res.IMG_BASE + name + (!this.moving || ++this.counter % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (this.toRight ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }


    protected final boolean hitAbove(GameObject gameObject) {
        return !(this.x + this.width < gameObject.getX() + HIT_MARGIN || this.x > gameObject.getX() + gameObject.getWidth() - HIT_MARGIN ||
                this.y < gameObject.getY() + gameObject.getHeight() || this.y > gameObject.getY() + gameObject.getHeight() + HIT_MARGIN);
    }

    protected final boolean hitAhead(GameElement character) {
        if (this.isToRight()) {
            return !(this.x + this.width < character.getX() || this.x + this.width > character.getX() + HIT_MARGIN ||
                    this.y + this.height <= character.getY() || this.y >= character.getY() + character.getHeight());
        } else {
            return false;
        }
    }

    protected final boolean hitBack(GameElement gameObject) {
        return !(this.x > gameObject.getX() + gameObject.getWidth() || this.x + this.width < gameObject.getX() + gameObject.getWidth() - HIT_MARGIN ||
                this.y + this.height <= gameObject.getY() || this.y >= gameObject.getY() + gameObject.getHeight());
    }


    protected final boolean hitBelow(GameElement gameObject) {
        if(gameObject instanceof GameObject){
            this.margin=HIT_MARGIN;
        }
        return !(this.x + this.width < gameObject.getX() + margin || this.x > gameObject.getX() + gameObject.getWidth() - margin ||
                this.y + this.height < gameObject.getY() || this.y + this.height > gameObject.getY() + margin);
    }

    public final boolean isNearby(GameElement obj) {
        return (this.x > obj.getX() - PROXIMITY_MARGIN && this.x < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.width > obj.getX() - PROXIMITY_MARGIN && this.x + this.width < obj.getX() + obj.getWidth() + PROXIMITY_MARGIN);
    }
}
