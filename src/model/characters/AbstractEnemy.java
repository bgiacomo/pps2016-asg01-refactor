package model.characters;

import java.awt.*;

import model.objects.GameObject;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractEnemy extends BasicCharacter implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(AbstractEnemy.class.getName());
    private static final int PAUSE = 15;
    private static final int OFFSET_X = 1;

    private int offsetX;

    public AbstractEnemy(int x, int y, int width, int height) {
        super(x, y, width, height);
        this.setToRight(true);
        this.setMoving(true);
        this.offsetX = OFFSET_X;
        this.startThread();
    }

    private void startThread(){
        Thread chronoEnemy = new Thread(this);
        chronoEnemy.start();
    }

    public final void move() {
        this.offsetX = isToRight() ? OFFSET_X : -OFFSET_X;
        this.setX(this.getX() + this.offsetX);
    }

    public final void run() {
        while (true) {
            if (!this.alive) {
                continue;
            }
            this.move();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                LOGGER.log(Level.WARNING, "AbstractEnemy exception!", e);

            }
        }
    }


    public final void contact(GameObject gameObject) {
        if (this.hitAhead(gameObject) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -OFFSET_X;
        } else if (this.hitBack(gameObject) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = OFFSET_X;
        }
    }

    public final void contact(BasicCharacter character) {
        if (this.hitAhead(character) && this.isToRight()) {
            this.setToRight(false);
        } else if (this.hitBack(character) && !this.isToRight()) {
            this.setToRight(true);
        }
        this.offsetX=OFFSET_X;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public abstract Image deadImage();
}
