package model;

import view.Main;

/**
 * Created by Giacomo on 11/03/2017.
 */
public class GameElement implements Element {

    protected int width;
    protected int height;
    protected int x;
    protected int y;

    public GameElement(int xGameElement, int yGameElement,int widthGameElement, int heightGameElement) {
        this.width = widthGameElement;
        this.height = heightGameElement;
        this.x = xGameElement;
        this.y = yGameElement;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public final int getY() {
        return this.y;
    }

    @Override
    public final int getWidth() {
        return this.width;
    }

    @Override
    public final int getHeight() {
        return this.height;
    }

    @Override
    public void setX(int xCharacter) {
        this.x=xCharacter;
    }

    @Override
    public final void setY(int yCharacter) {
        this.y=yCharacter;
    }

    @Override
    public void move() {
        if (Main.getScene().getxPosition() >= 0) {
            this.x = this.x - Main.getScene().getMovement();
        }
    }
}
