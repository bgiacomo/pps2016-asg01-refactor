package controller;

import model.characters.Mario;
import model.characters.Mushroom;
import model.characters.Turtle;
import model.objects.Coin;
import model.objects.GameObject;
import utils.Res;
import view.Audio;
import view.Main;
import view.Score;
import java.util.List;

/**
 * Created by Giacomo on 13/03/2017.
 */
public class ContactController {

    public ContactController() {
    }

    public final void checkContactWithCoin(List<Coin> coins, Mario mario) {
        for (int i = 0; i < coins.size(); i++) {
            if (mario.contactCoin(coins.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                Score.getInstance().incrementScore();
                Main.getLabel().setText("  Score:  "+Score.getInstance().getScore());
                coins.remove(i);
            }
        }
    }

    public final void checkContactsWithObjects(List<GameObject> objects, Mario mario, Mushroom mushroom, Turtle turtle) {

        for (GameObject object : objects) {
            if (mario.isNearby(object)) {
                mario.contact(object);
            }

            if (mushroom.isNearby(object)) {
                mushroom.contact(object);
            }

            if (turtle.isNearby(object)) {
                turtle.contact(object);
            }
        }
    }

    public final void checkContactAmongstCharacters(Mushroom mushroom,Turtle turtle,Mario mario) {
        if (mushroom.isNearby(turtle)) {
            mushroom.contact(turtle);
        }
        if (turtle.isNearby(mushroom)) {
            turtle.contact(mushroom);
        }
        if (mario.isNearby(mushroom)) {
            mario.contact(mushroom);
        }
        if (mario.isNearby(turtle)) {
            mario.contact(turtle);
        }
    }
}
