package controller;

import model.characters.Mushroom;
import model.characters.Turtle;
import model.objects.GameObject;
import model.objects.Coin;

import java.util.List;

/**
 * Created by Giacomo on 13/03/2017.
 */
public class ObjectsMoverController {

    public ObjectsMoverController() {
    }

    public final void moveTurtleAndMushroom(Mushroom mushroom, Turtle turtle) {
        mushroom.move();
        turtle.move();
    }

    public final void movePieces(List<Coin> coins) {
        for (int i = 0; i < coins.size(); i++) {
            coins.get(i).move();
        }
    }

    public final void moveObjects(List<GameObject> objects) {
        for (int i = 0; i < objects.size(); i++) {
            objects.get(i).move();
        }
    }
}
