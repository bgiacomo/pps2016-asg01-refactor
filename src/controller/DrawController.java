package controller;

import java.awt.*;
import model.objects.*;
import model.characters.*;
import utils.Res;
import java.util.List;

/**
 * Created by Giacomo on 12/03/2017.
 */
public class DrawController {

    private static final int TURTLE_FREQUENCY = 45;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int MARIO_FREQUENCY = 25;
    private static final int CASTLE_X_POS = 4850;
    private static final int CASTLE_Y_POS = 145;
    private static final int FLAG_Y_POS = 115;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_INITIAL_POSITION_X = 10;
    private static final int CASTLE_INITIAL_POSITION_Y = 95;
    public static final int STARTING_POINT_INITIAL_POSITION_X = 220;
    public static final int STARTING_POINT_INITIAL_POSITION_Y = 234;


    public DrawController() {
    }

    public final void drawTurtle(Graphics g2, Turtle turtle) {
        if (turtle.isAlive()) {
            g2.drawImage(turtle.walk(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), turtle.getX(), turtle.getY(), null);
        } else {
            g2.drawImage(turtle.deadImage(), turtle.getX(), turtle.getY() + TURTLE_DEAD_OFFSET_Y, null);
        }
    }

    public final void drawMushroom(Graphics g2, Mushroom mushroom) {
        if (mushroom.isAlive()) {
            g2.drawImage(mushroom.walk(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), mushroom.getX(), mushroom.getY(), null);
        } else {
            g2.drawImage(mushroom.deadImage(), mushroom.getX(), mushroom.getY() + MUSHROOM_DEAD_OFFSET_Y, null);
        }

    }

    public final void drawMario(Graphics g2, Mario mario) {
        if (mario.isJumping()) {
            g2.drawImage(mario.doJump(), mario.getX(), mario.getY(), null);
        } else {
            g2.drawImage(mario.walk(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), mario.getX(), mario.getY(), null);
        }
    }

    public final void drawBackground(Graphics g2, Background background) {
        g2.drawImage(background.getImage(), background.getX(), 0, null);

    }

    public final void drawCastle(Graphics g2, Castle castle, int xPosition) {
        g2.drawImage(castle.getImage(), CASTLE_INITIAL_POSITION_X - xPosition, CASTLE_INITIAL_POSITION_Y, null);

    }

    public final void drawStartPoint(Graphics g2, StartPoint start, int xPosition) {
        g2.drawImage(start.getImage(), STARTING_POINT_INITIAL_POSITION_X - xPosition, STARTING_POINT_INITIAL_POSITION_Y, null);
    }

    public final void drawFinalCastle(Graphics g2, FinalCastle finalCastle, int xPosition) {
        g2.drawImage(finalCastle.getImage(), CASTLE_X_POS - xPosition, CASTLE_Y_POS, null);
    }

    public final void drawFlag(Graphics g2,Flag flag,int xPosition) {
        g2.drawImage(flag.getImage(), FLAG_X_POS - xPosition, FLAG_Y_POS, null);
    }

    public final void drawPieces(Graphics g2, List<Coin> coins) {
        for (int i = 0; i < coins.size(); i++) {
            g2.drawImage(coins.get(i).imageOnMovement(), coins.get(i).getX(),
                    coins.get(i).getY(), null);
        }
    }

    public final void drawObjects(Graphics g2,List<GameObject> objects) {
        for (int i = 0; i < objects.size(); i++) {
            g2.drawImage(objects.get(i).getImageObject(), objects.get(i).getX(),
                    objects.get(i).getY(), null);
        }
    }

}
