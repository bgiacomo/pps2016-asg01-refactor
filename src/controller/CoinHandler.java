package controller;

import model.objects.Coin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giacomo on 13/03/2017.
 */
public class CoinHandler {

    private List<Coin> coins;

    public CoinHandler() {
        this.coins = new ArrayList<>();
        this.createCoins();
    }

    public final List<Coin> getPiecesList() {
        return coins;
    }

    private void createCoins() {
        this.coins.add(new Coin(402, 145));
        this.coins.add(new Coin(1202, 140));
        this.coins.add(new Coin(1272, 95));
        this.coins.add(new Coin(1342, 40));
        this.coins.add(new Coin(1650, 145));
        this.coins.add(new Coin(2650, 145));
        this.coins.add(new Coin(3000, 135));
        this.coins.add(new Coin(3400, 125));
        this.coins.add(new Coin(4200, 145));
        this.coins.add(new Coin(4600, 40));
    }
}
