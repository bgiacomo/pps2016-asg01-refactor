package controller;

import model.objects.Block;
import model.objects.GameObject;
import model.objects.Tunnel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giacomo on 13/03/2017.
 */
public class ObjectsHandler {

    private List<GameObject> objects;

    public ObjectsHandler() {
        objects = new ArrayList<>();
        this.addTunnels();
        this.addBlocks();
    }

    public final List<GameObject> getObjectsList() {
        return objects;
    }

    private void addTunnels(){
        this.objects.add(new Tunnel(600, 230));
        this.objects.add(new Tunnel(1000, 230));
        this.objects.add(new Tunnel(1600, 230));
        this.objects.add(new Tunnel(1900, 230));
        this.objects.add(new Tunnel(2500, 230));
        this.objects.add(new Tunnel(3000, 230));
        this.objects.add(new Tunnel(3800, 230));
        this.objects.add(new Tunnel(4500, 230));
    }

    private void addBlocks(){
        this.objects.add(new Block(400, 180));
        this.objects.add(new Block(1200, 180));
        this.objects.add(new Block(1270, 170));
        this.objects.add(new Block(1340, 160));
        this.objects.add(new Block(2000, 180));
        this.objects.add(new Block(2600, 160));
        this.objects.add(new Block(2650, 180));
        this.objects.add(new Block(3500, 160));
        this.objects.add(new Block(3550, 140));
        this.objects.add(new Block(4000, 170));
        this.objects.add(new Block(4200, 200));
        this.objects.add(new Block(4300, 210));
    }
}
