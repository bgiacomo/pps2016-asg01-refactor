package Test;

import org.junit.Assert;
import org.junit.Test;
import view.Score;

/**
 * Created by Giacomo on 15/03/2017.
 */
public class ScoreTest {


    public static final int NUMBER_OF_INCRESEAS = 5;

    @Test
    public void incrementScore() throws Exception {

        for (int i = 0; i< NUMBER_OF_INCRESEAS; i++){
            Score.getInstance().incrementScore();
        }
        Assert.assertTrue(Score.getInstance().getScore()== NUMBER_OF_INCRESEAS);
    }

    @Test
    public void resetScore() throws Exception {
        Score.getInstance().resetScore();
        Assert.assertTrue(Score.getInstance().getScore()==0);

    }

}